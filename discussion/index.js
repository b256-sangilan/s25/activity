// console.log("Happy Thursday");

// [SECTION] JSON
	/*
	- JSON stands for Javascript Object Notation which is a popular data format which applications use to communicate with one another.
	- JSON is named after the JS Objects Because it looks alike. The both have a {} and key-value pairs. However, the keys of a JSON object is sorrounded by "".
*/

// JSON Object

let sampleJSON = `{
	"name": "Meredith Grey",
	"age": 20,
	"address": {
		"state": "Seattle",
		"country": "USA"
	}
}`
console.log(sampleJSON);
console.log(typeof sampleJSON);

// [SECTION] JSON Methods

// JSON.parse() will return the given JSON as a JS Object.
// JSON -> JSON.parse

let sampleJSObject = JSON.parse(sampleJSON);
console.log(sampleJSObject);
console.log(typeof sampleJSObject);

// JSON.stringify() will return the given JS Object as a stringified JSON format.
// JS Object -> JSON.stringify -> JSON

let sampleJSObject2 = {
	username: "mergrey",
	password: "grey123",
	age: 20
};
let sampleJSONFormat = JSON.stringify(sampleJSObject2);
console.log(sampleJSONFormat);
console.log(typeof sampleJSONFormat);

// [SECTION] JSON Array

let sampleJSONArr = `[
	{
		"email" : "evanbuckley@gmail.com",
		"password": "evan123",
		"isAdmin": false
	},
	{
		"email" : "janebuckley@gmail.com",
		"password": "jane123",
		"isAdmin": false
	}
]`

console.log(sampleJSONArr);

// convert the JSON file into an array by using JSON.parse();

let parsedArr = JSON.parse(sampleJSONArr);
console.log(parsedArr);

// delete the last item in the array

parsedArr.pop();
console.log(parsedArr);

// convert the JS Object into a JSON format by using JSON.stringify()

let stringfyArr = JSON.stringify(
	parsedArr);
console.log(stringfyArr);

/*
	Do: Add double quotes to your keys
	Do: Add colons after each key
	
	Don't: add excessive commas
	Don't: forget to close double quotes and curly braces

	When you parse JSON with an erratic format, it produces an error
*/